const CANVAS = document.getElementById("myCanvas");
      const CTX = CANVAS.getContext("2d");
      const SPEED = 8; // Movement speed in pixels per frame
      const JOUEUR_HEIGHT = 40;
      const JOUEUR_WIDTH = 16;
      const LIMITE_SOURIS = CANVAS.height - 250; //Limite de jeu.
      const SMALL_RADIUS = 5;
      const BIG_RADIUS = 10;
      const HUGE_RADIUS = 45;
      const TIR_MAX_DISTANCE = CANVAS.height - JOUEUR_HEIGHT;
      let projectileFired = false;
      let distanceTir = 0;
      let mousePosX = 0;
      let mousePosY = 0;
      let bulletEndPosX = 0;
      let bulletEndPosY = 0;
      let clickStartTime = 0;
      let clickDuration = 0;
      let monstrPosx = 0;
      let monstrPosy = 0;
      let monstrWidth = 0;
      let monstrHeight = 0;
      let monstrColor = "";
      let essais = 5;

      randomizeMonster();

      drawRect(
        CANVAS.width / 2,
        CANVAS.height - JOUEUR_HEIGHT,
        JOUEUR_WIDTH,
        JOUEUR_HEIGHT,
        "blue"
      ); //Joueur.
      drawLine(0, LIMITE_SOURIS, CANVAS.width, LIMITE_SOURIS, "grey"); //Limite de jeu.
      drawRect(monstrPosx, monstrPosy, monstrWidth, monstrHeight, monstrColor); //Monstreudeupoche.

      //Gestion du déplacement du joueur à la souris :
      CANVAS.addEventListener("mousemove", function (event) {
        let rect = CANVAS.getBoundingClientRect();

        //Récupération de la position de la souris :
        mousePosX = event.clientX - rect.left;
        mousePosY = event.clientY - rect.top;

        if (mousePosY > LIMITE_SOURIS && projectileFired == false) {
          //s'assurer qu'on joue sous le trait et qu'on ai qu'un tir à la fois.
          clearCanvas();
          drawLine(0, LIMITE_SOURIS, CANVAS.width, LIMITE_SOURIS, "grey"); //Limite de jeu.
          drawRect(
            mousePosX,
            CANVAS.height - JOUEUR_HEIGHT,
            JOUEUR_WIDTH,
            JOUEUR_HEIGHT,
            "blue"
          ); //position x du joueur selon la souris.
          drawRect(
            monstrPosx,
            monstrPosy,
            monstrWidth,
            monstrHeight,
            monstrColor
          ); //Monstreudeupoche.
        }
      });

      //pour connaitre plus tard le temps d'appui sur le clic souris :
      CANVAS.addEventListener("mousedown", function (event) {
        clickStartTime = Date.now();
      });

      //Gestion du tir, lorsque le joueur relache le clic souris :
      CANVAS.addEventListener("mouseup", function (event) {
        clickDuration = Date.now() - clickStartTime; //temps d'appui sur le clic souris, en millisecondes.
        // console.log("User stayed on the mouse click for " + clickDuration + " milliseconds");
        clickDuration < 1600
          ? (distanceTir = clickDuration / 2)
          : (distanceTir = TIR_MAX_DISTANCE); //tir max après 1.6 seconde.
        let finalMousePosX = mousePosX; //position de la souris au moment du tir.
        let bulletPosX = finalMousePosX + JOUEUR_WIDTH / 2; // Initial bullet x position
        let bulletPosY = CANVAS.height - JOUEUR_HEIGHT; // Initial bullet y position
        let distance = distanceTir;
        bulletEndPosY = CANVAS.height - JOUEUR_HEIGHT - distanceTir; //là où le tir va finir.
        let bulletRadius = SMALL_RADIUS;

        // Animate the movement
        if (mousePosY > LIMITE_SOURIS && projectileFired == false) {
          //pour n'avoir qu'un tir à la fois.
          let animation = setInterval(function () {
            if (distance > 0) {
              projectileFired = true;
              bulletPosY -= SPEED; //Déplace la balle vers le haut de l'écran.
              clearCanvas();
              // drawDot(finalMousePosX + JOUEUR_WIDTH/2, CANVAS.height - JOUEUR_HEIGHT - distanceTir, BIG_RADIUS, "purple"); //pour tricher un peu
              drawLine(0, LIMITE_SOURIS, CANVAS.width, LIMITE_SOURIS, "grey"); //Limite de jeu
              drawRect(
                finalMousePosX,
                CANVAS.height - JOUEUR_HEIGHT,
                15,
                JOUEUR_HEIGHT,
                "blue"
              ); //Dessin du joueur à la position au moment du tir
              drawRect(
                monstrPosx,
                monstrPosy,
                monstrWidth,
                monstrHeight,
                monstrColor
              ); //Monstreudeupoche
              if (distance > distanceTir / 2) {
                //pour l'impression de parabole : agrandir puis rapetisser la taille de la balle.
                bulletRadius += 0.6;
              } else {
                bulletRadius -= 0.6;
              }
              drawDot(bulletPosX, bulletPosY, bulletRadius, "blue"); // Draw the blue dot in its new position
              distance -= SPEED; //On réduit la distance à parcourir.
            } else {
              clearInterval(animation); // Stop the animation when the blue dot reaches its target
              clearCanvas();
              drawDot(bulletPosX, bulletEndPosY, HUGE_RADIUS, "grey"); //Dessin du filet à Monstreudeupoche qui était contenu dans la balle et qui s'est deployé.
              drawLine(0, LIMITE_SOURIS, CANVAS.width, LIMITE_SOURIS, "grey"); //Limite de jeu
              drawRect(
                finalMousePosX,
                CANVAS.height - JOUEUR_HEIGHT,
                15,
                JOUEUR_HEIGHT,
                "blue"
              ); //joueur
              drawRect(
                monstrPosx,
                monstrPosy,
                monstrWidth,
                monstrHeight,
                monstrColor
              ); //Monstreudeupoche

              if (
                bulletPosX + HUGE_RADIUS > monstrPosx && //Si on a visé juste...
                bulletPosX - HUGE_RADIUS < monstrPosx + monstrWidth &&
                bulletEndPosY + HUGE_RADIUS > monstrPosy && //NB : pos rectangle = coin haut-gauche du rectangle
                bulletEndPosY - HUGE_RADIUS < monstrPosy + monstrHeight
              ) {
                setTimeout(function () {
                  alert("Bien joué ! Monstreudeupoche attrapé !"); //...on gagne.
                }, 5);
                randomizeMonster(); //Nouveau Monstreudeupoche à attraper.
                essais = 5; //Réinitialisation du nombre d'essais.
                document.getElementById("essais").innerHTML =
                  "Monstroballs : " + essais;
              } else {
                essais--;
                document.getElementById("essais").innerHTML =
                  "Monstroballs : " + essais;
                if (essais == 0) {
                  setTimeout(function () {
                    alert("T'es nul, achète du skill lol !");
                  }, 100);
                  randomizeMonster();
                  essais = 5;
                }
              }
              projectileFired = false; //Le tir est fini, on peut tirer un autre projectile.
            }
            // document.getElementById("debug2").innerHTML = "mousePosX: " + mousePosX + ", mousePosY: " + mousePosY;
            // document.getElementById("debug1").innerHTML = "distance: " + distance;
            // document.getElementById("debug1").innerHTML = "xB-rB : " + (bulletPosX - HUGE_RADIUS) + ". xB+rB : " + (bulletPosX + HUGE_RADIUS);
            // document.getElementById("debug2").innerHTML = "yB-rB : " + (bulletEndPosY - HUGE_RADIUS) + ". yB+rB : " + (bulletEndPosY + HUGE_RADIUS);
          }, 12); //last parameter is the speed of the animation
        }
      });

      /** Dessine un rectangle sur le canvas avec la position, la taille et la couleur spécifiées.
       * @param {number} posX - La coordonnée x du coin supérieur gauche du rectangle
       * @param {number} posY - La coordonnée y du coin supérieur gauche du rectangle
       * @param {number} width - La largeur du rectangle
       * @param {number} height - La hauteur du rectangle
       * @param {string} color - La couleur du rectangle
       */
      function drawRect(posX, posY, width, height, color) {
        CTX.beginPath();
        CTX.rect(posX, posY, width, height);
        CTX.fillStyle = color;
        CTX.fill();
        CTX.strokeStyle = "black";
        CTX.stroke();
        CTX.closePath();
      }

      /** Dessine un point sur le canvas avec la position, le rayon et la couleur spécifiés.
       * @param {number} posX - La coordonnée x du centre du point
       * @param {number} posY - La coordonnée y du centre du point
       * @param {number} radius - Le rayon du point
       * @param {string} color - La couleur du point
       */
      function drawDot(posX, posY, radius, color) {
        CTX.beginPath();
        CTX.arc(posX, posY, radius, 0, Math.PI * 2, false);
        CTX.fillStyle = color;
        CTX.fill();
        CTX.strokeStyle = "#424242";
        CTX.stroke();
        CTX.closePath();
      }

      /** Dessine une ligne sur le canvas avec les positions des deux points et la couleur spécifiées.
       * @param {number} dot1PosX - La coordonnée x du premier point
       * @param {number} dot1PosY - La coordonnée y du premier point
       * @param {number} dot2PosX - La coordonnée x du deuxième point
       * @param {number} dot2PosY - La coordonnée y du deuxième point
       * @param {string} color - La couleur de la ligne
       */
      function drawLine(dot1PosX, dot1PosY, dot2PosX, dot2PosY, color) {
        CTX.beginPath();
        CTX.moveTo(dot1PosX, dot1PosY); //départ ligne
        CTX.lineTo(dot2PosX, dot2PosY); //arrivée ligne
        CTX.strokeStyle = color;
        CTX.stroke(); // Draw the line
        CTX.closePath();
      }

      /** Efface tout le contenu du canvas.
       */
      function clearCanvas() {
        /**
         * Efface une région rectangulaire spécifiée du canvas.
         * @param {number} x - La coordonnée x du coin supérieur gauche de la région à effacer
         * @param {number} y - La coordonnée y du coin supérieur gauche de la région à effacer
         * @param {number} width - La largeur de la région à effacer
         * @param {number} height - La hauteur de la région à effacer
         */
        CTX.clearRect(0, 0, CANVAS.width, CANVAS.height);
      }

      /** Randomsize la position, la forme et la couleur du Monstreudeupoche.
       */
      function randomizeMonster() {
        monstrPosx = Math.floor(Math.random() * CANVAS.width);
        monstrPosy = Math.floor(
          Math.random() * (CANVAS.height - (CANVAS.height - LIMITE_SOURIS))
        );
        monstrWidth = Math.floor(Math.random() * 50) + 10; // Random width between 10 and 60
        monstrHeight = Math.floor(Math.random() * 50) + 10; // Random height between 10 and 60
        monstrColor = "#" + Math.floor(Math.random() * 16777215).toString(16); // Random hex color
      }
