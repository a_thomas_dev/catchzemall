# Catch Zem All
![HTML](https://img.shields.io/badge/HTML-white?style=plastic&logo=html5&logoColor=white&color=%23E34F26)
![JavaScript](https://img.shields.io/badge/JavaScript-white?style=plastic&logo=javascript&logoColor=white&color=%23F7DF1E)

## Présentation
Un prototype simple de jeu de tir en 2D vue de dessus, utilisant la balise HTML "canvas" et du JavaScript.  
[Vous pouvez y jouer ici](https://catchzemall.vercel.app/).

## But du jeu
Il faut attraper en 5 essais maximum le "Monstreudeupöche" (la forme rectangulaire dans le plan supérieur). Pour cela il faut placer le joueur (le rectangle bleu du plan inférieur) dessous (en déplacant la souris dans le plan inférieur), puis appuyer sur le clic-gauche de la souris aussi longtemps que la cible est loin, pour lancer une balle attrape-Monstreudeupöche qui se déploira en filet à la fin de sa course.

Bonne chance ! Il ne peut en rester qu'un (mmh ah non c'est autre chose ça) !
